import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { HomeComponent } from "./home/home.component";
import { SharedModule } from "src/app/shared/shared.module";
import { AppListComponent } from "./app-list/app-list.component";
import { AppDescriptionComponent } from "./app-description/app-description.component";
import { AppDetailsComponent } from "./app-details/app-details.component";

import { AppRoutingModule } from "../../app-routing.module";
import { UserProfileComponent } from './user-profile/user-profile.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgBootstrapFormValidationModule } from 'ng-bootstrap-form-validation';
import { ConfirmEqualValidatorDirective } from 'src/app/_helpers/validate-password.directive';
import {FroalaEditorModule, FroalaViewModule} from 'angular-froala-wysiwyg';
import { AppToggleComponent } from 'src/app/shared/components/app-toggle.component';


// import { AmplifyAngularModule, AmplifyService } from 'aws-amplify-angular';


@NgModule({
  declarations: [
    HomeComponent,
    AppListComponent,
    AppDescriptionComponent,
    AppDetailsComponent,
    UserProfileComponent,
    ConfirmEqualValidatorDirective,
    AppToggleComponent
  ],
  imports: [AppRoutingModule, CommonModule, SharedModule,FormsModule,
    ReactiveFormsModule,NgBootstrapFormValidationModule.forRoot(),
    FroalaEditorModule.forRoot(), FroalaViewModule.forRoot()
  ],
  exports: [HomeComponent, AppListComponent, AppDescriptionComponent],
  providers:[]

})
export class MainModule {}
