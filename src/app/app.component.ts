import { Component } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  constructor(public translate: TranslateService) {
    translate.setDefaultLang("en");
  }
  // Method to change the language.
  switchLanguage(language: string) {
    this.translate.use(language);
  }
  title = "scythe";
}
